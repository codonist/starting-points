
## Table of contents

### Wordpress CheatSheet

#### Wordpress Installation :
1. Open xxamp or wamp and run appache and sql server
2. Open phpmyadmin and create database
3. Download latest version of wordprsss from [https://wordpress.org/download/](https://wordpress.org/download/)
4. Extract the zip in htdocs folder
5. Rename folder from wordpress to project-name

#### Wordpress Theme Development:

1) Basic Conversion
2) CPTs
3) Loops, Hooks, Enqueing
4) Theme options
5) Visual composer
---
1) Basic Convertion
	- style.css
	- index.php
	- header.php
	- sidebar.php
	- footer.php
	- comments.php

	(optional pages)
	- category.php (category pages)
	- single.php (single posts)
	- 404.php (site error 404)
	- search.php
	- home.php
	- functions.php
	- page.php


> style.css
```css
/*
Theme Name: [A unique theme name here]
Theme URI: [The theme website]
Description: [A description]
Author: aitch
Author URI: [Your website].[Any other comments].
Version: 1.0 
Tags: minimalistic, simple, widgets, sidebar, elegant 
*/
```
note : Make sure the theme’s name differs from any other themes you have installed, or it will cause problems!

> index.php

```php
<?php bloginfo('stylesheet_url'); ?>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css"/>
<link rel="stylesheet" href="http://example/blog/wp-content/themes/style.css" type="text/css" />

<?php bloginfo('template_directory') ?>/assets/
```

> header.php

```php
<?php get_header(); ?>
```

> footer.php
```php
<?php get_footer(); ?>
```
> sidebar.php
```php
<?php get_sidebar(); ?>
```

> template tags
```php
<?php language_attributes();  =  lang="en-US"
```


note: For blogs, it’s a good idea to include links to your RSS feed and pingback URL in your head, as these will be automatically recognized by many browsers. Both of these links can be added using the

```php
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" /><link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
```
note: This tag will pull in any JavaScript files or style sheets required by the WordPress plugins you’re using. This is vital, as those plugins may fail to function as intended. All you needis the line:
```php
<?php wp_head(); ?>

<title><?php bloginfo('name'); ?> <?php wp_title('-'); ?></title>
```

``` get_option('home'); ``` : the URL of the blog’s home page

``` bloginfo('rss2_url'); ``` : the URL of the blog’s RSS feed

``` bloginfo('description'); ``` : the description of the blog, as defined in the WordPress settings

```<?php wp_list_categories(); ?>```

```<?php wp_list_pages(); ?>```

```wp_list_authors();``` : lists all the blog’s authors as li elements. If they have authored posts, their name will be a link to a page showing all their posts.

```wp_list_bookmarks();``` : outputs the links that have been added in the Links section of the administration panel, and is also wrapped in li tags.

```wp_tag_cloud();``` : displays a cloud of all the tags that have been added to your posts.

> Loop:
```php
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h2><small><?php the_time('F jS, Y') ?> by <?php the_author() ?> </small>

the_permalink() : the URL of the permanent hyperlink to the post.

the_title() : the post’s title.

the_time('F jS, Y') : displays the post’s date in “January 1st, 2010” format.

the_author() : displays the name of and links to the archive for the user who wrote the post.

the_content() : inserts the post content. There’s no need to place this inside <p></p> tags, as this will be done automatically.

the_category() : displays the name of and links to the archive of the post’s category.

:end of loop

<?php endwhile; ?>


<div class="navigation">
	<div class="alignleft">
		<?php previous_posts_link('&laquo; Previous Entries') ?>
	</div>
	<div class="alignright">
		<?php next_posts_link('Next Entries &raquo;') ?>
	</div>
</div>

<?php else: ?><p>Sorry, there are no posts to display.</p><?php endif; ?>
```
---

```php

<?php

if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/ReduxFramework/ReduxCore/framework.php' ) ) {
   require_once( dirname( __FILE__ ) . '/ReduxFramework/ReduxCore/framework.php' );
}
if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/ReduxFramework/sample/sample-config.php' ) ) {
   require_once( dirname( __FILE__ ) . '/ReduxFramework/sample/sample-config.php' );
}

if(!isset($content_width)){
    $content_width = 660;
}
function foldwp_setup(){
    add_theme_support('automatic-feed-links');
    add_theme_support('title-tag');
}
add_action('after_setup_theme','foldwp_setup');
function add_theme_scripts() {
   wp_enqueue_style( 'style', get_template_directory_uri() .'/style.css' );
   wp_enqueue_style( 'bootstrap',  get_template_directory_uri() .'/css/bootstrap.min.css' );
   wp_enqueue_style( 'slider', get_template_directory_uri() . '/css/slider.css', array(), '1.1', 'all');
   wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array ( 'jquery' ), true);
   wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array ( 'jquery' ), true);
   wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-1.11.2.min.js', array ( 'jquery' ), true);

   if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
       wp_enqueue_script( 'comment-reply' );
   }

}
     add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

      //enqueues our locally supplied font awesome stylesheet
    function enqueue_our_required_stylesheets(){
   wp_enqueue_style('font-awesome', get_stylesheet_directory_uri() . '/css/font-awesome.min.css');
  }
    add_action('wp_enqueue_scripts','enqueue_our_required_stylesheets');

   //   dynamic menu
  // Register Custom Navigation Walker
  require_once('wp_bootstrap_navwalker.php');
  register_nav_menus( array(
   'primary' => __( 'header-menu', 'built-smart' ),
  ) );

    add_theme_support( 'post-thumbnails' );

```
---

Menu:
<?php wp_nav_menu(); ?>

For Index:
<?php get_header(); ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
------------------------------------------------------

For Widgets:

1. Create functions.php with: (Definitions)
```php
<?php
if ( function_exists('register_sidebar') )
    register_sidebar(array(
'name' => __( 'Secondary Widget Area', 'twentyten' ),
'id' => 'secondary-widget-area',        
'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="title">',
        'after_title' => '</div>',
    ));
?>
```
2. Enter this under Sidebar.php: (Calling)

```php
<?php if ( !function_exists('dynamic_sidebar')
        || !dynamic_sidebar() ) : ?>
<?php endif; ?>
```
Call any sidebar:
```php
<?php if ( ! dynamic_sidebar( 'content-footer-widgets' ) ) : ?>
	<?php endif; ?>
```


For Displaying a specific Post:
```php
<?php get_a_post(31); ?>
<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
Image:
<?php the_post_thumbnail( array (250,150) );?>
```

Including CSS file:
```php
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<?php bloginfo(‘url’); ?>
```

To filter any Loop by its CATEGORY:
```php
<?php
query_posts('category_name=cat1&showposts=3');
while (have_posts()) : the_post(); ?>

Rest of the Loop

<?php endwhile; ?>
```

Child Template new code:
```css
/*
Theme Name:     Twenty Eleven Child
Theme URI:      http: //example.com/
Description:    Child theme for the Twenty Eleven theme 
Author:         Your name here
Author URI:     http: //example.com/about/
Template:       twentyeleven
Version:        0.1.0
*/
```